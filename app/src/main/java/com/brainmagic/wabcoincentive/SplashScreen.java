package com.brainmagic.wabcoincentive;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Timer;

import alertbox.Alertbox;
import api.APIService;
import model.checkapproved.CheckApproved;
import model.register.RegisterData;
import retrofit.RetrofitClient3;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends AppCompatActivity {
    int noofsize = 5;
    int count = 0;
    Timer timer;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    public ProgressDialog loadDialog;
    public Connection connection;
    public Statement stmt;
    private String android_id;
    private int versionCode;
    private String str_usertype, str_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);

        PackageManager pm = getPackageManager();
        PackageInfo pi = null;
        str_usertype = myshare.getString("usertype", "");
        str_phone = myshare.getString("phone", "");
        try {
            pi = pm.getPackageInfo(getPackageName(), 0);
            versionCode = pi.versionCode;
            Log.v("Version name", Integer.toString(versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        edit.putInt("VERSION", versionCode).commit();
        if (!myshare.getBoolean("isregistration", false)) {
            //CheckInternetForID();
            setContentView(R.layout.activity_splash_screen);
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub

                    Intent mainIntent = new Intent(SplashScreen.this,
                            RegisterActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(mainIntent);
                    // }

                }
            }, 3000);

				/*Intent reg = new Intent(HomePage.this,HomePage.class);
				reg.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
				startActivity(reg);*/

            //     Intent reg = new Intent(SplashScreen.this, RegisterActivity.class);
            //reg.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            //  startActivity(reg);

        } else {
            setContentView(R.layout.activity_splash_screen);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    if (!str_phone.isEmpty() && !str_usertype.isEmpty()) {
                        validateApproval();
                    } else {
                        Intent mainIntent = new Intent(SplashScreen.this,
                                MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(mainIntent);
                    }
                }
            }, 3000);
        }

    }


    private void validateApproval() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "Checking For Approval", "Please wait...", false, false);
        APIService service = RetrofitClient3.getApiService();
        Call<CheckApproved> call1 = service.CHECK_APPROVED_CALL(str_phone, str_usertype);
        call1.enqueue(new Callback<CheckApproved>() {
            @Override
            public void onResponse(Call<CheckApproved> call, Response<CheckApproved> response) {
                try {
                    if (response.body().getResult().equals("Approved")) {
                        loading.dismiss();
                        Intent mainIntent = new Intent(SplashScreen.this,
                                MainActivity.class);
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(mainIntent);
                    } else if (response.body().getResult().equals("Not Approved")) {
                        Alertbox alertbox = new Alertbox(SplashScreen.this);
                        alertbox.showNegativebox("Your Account is not yet Activated. Please Wait for Approval.");
                        loading.dismiss();
                    } else {
                        Alertbox alertbox = new Alertbox(SplashScreen.this);
                        alertbox.showNegativebox("Your Account is not yet Activated. Please Wait for Approval.");
                        loading.dismiss();
                    }

                } catch (Exception e) {
                    loading.dismiss();
                    e.printStackTrace();
                    Toast.makeText(SplashScreen.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckApproved> call, Throwable t) {
                try {
                    loading.dismiss();
                    //  Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    Toast.makeText(SplashScreen.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
}

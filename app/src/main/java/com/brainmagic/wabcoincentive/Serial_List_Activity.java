package com.brainmagic.wabcoincentive;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.mancj.materialsearchbar.MaterialSearchBar;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import model.SerialData;
import model.SerialList;
import model.checkPartId.CheckPartId;
import model.checkPartId.CheckPartIdList;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Serial_List_Activity extends AppCompatActivity {
    private Alertbox box = new Alertbox(Serial_List_Activity.this);
    private String mSixDigit;
    private ListView mListSerial;
    private Button mSelect;
    private Vibrator vibrator;
    private String mSerialPosition,userType,mAlertmsg;
    private boolean IsSelected;
    private     ArrayList<String> adap = new ArrayList<>();
    private MaterialSearchBar mSearchBar;
    private ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serial__list_);
        mSixDigit = getIntent().getStringExtra("mSixDigit");
        userType = getIntent().getStringExtra("usertype");
        mListSerial = (ListView) findViewById(R.id.list);
        mSelect = (Button) findViewById(R.id.select);
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        mSearchBar = (MaterialSearchBar) findViewById(R.id.searchBar);

        mListSerial.setAdapter(adapter);
//        adap.add("120358");
//        adap.add("111111");
//        adap.add("222222");
//        adap.add("3333333");
//        adap.add("9874566");



        adapter =  new ArrayAdapter<String>(
                Serial_List_Activity.this,
                android.R.layout.simple_list_item_single_choice,
                adap);
        mListSerial.setAdapter(adapter);

        mListSerial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IsSelected = true;
                mSerialPosition = parent.getAdapter().getItem(position).toString();

            }
        });

        mSearchBar.addTextChangeListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        if (IsSelected) {
                            IsSelected = false;

//                            Intent a = new Intent(Serial_List_Activity.this, Sacn.class);
//                            a.putExtra("Serial id", mSerialPosition);
//                            startActivity(a);
                            final AlertDialog alertDialog;
                            alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
                            LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.successpopup, null);
                            alertDialog.setView(dialogView);
                            Button Back = (Button) dialogView.findViewById(R.id.okbtn);

                            Back.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //progressDialog.dismiss();
                                    alertDialog.dismiss();

                                    Intent i = new Intent(Serial_List_Activity.this,MechanicResult.class);
                                    i.putExtra("mechincentiveamount","20");
                                    i.putExtra("dateofscan","20/3/2019");
                                    i.putExtra("dealername","Velu");
                                    i.putExtra("dealercity","Madurai");
                                    i.putExtra("dealerstate","Tamilnadu");
                                    i.putExtra("alert","noalert");
                                    startActivity(i);
                                }
                            });
                            alertDialog.show();

//                            if(mSerialPosition.equals("120358")) {
//                                final AlertDialog alertDialog;
//                                alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
//                                LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
//                                View dialogView = inflater.inflate(R.layout.successpopup, null);
//                                alertDialog.setView(dialogView);
//                                alertDialog.setCancelable(false);
//                                Button Back = (Button) dialogView.findViewById(R.id.okbtn);
//
//                                Back.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        //progressDialog.dismiss();
//                                        alertDialog.dismiss();
//                                        Intent i = new Intent(Serial_List_Activity.this,RetailerResult.class);
//                                        i.putExtra("incentiveamount","50  (30/20)");
//                                        i.putExtra("sourceofpurchase","WABCO-India");
//                                        i.putExtra("state","Tamilnadu");
//                                        i.putExtra("city","Chennai");
//                                        i.putExtra("alert","noalert");
//                                        startActivity(i);
//                                    }
//                                });
//                                alertDialog.show();
//
//
//                            } else if (mSerialPosition.equals("111111")) {
//                                if(Build.VERSION.SDK_INT >=26){
//                                    vibrator.vibrate(VibrationEffect.createOneShot(200,VibrationEffect.DEFAULT_AMPLITUDE));
//                                }else {
//                                    vibrator.vibrate(200);
//                                }
//                                final AlertDialog alertDialog;
//                                alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
//                                LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
//                                View dialogView = inflater.inflate(R.layout.popupdealer, null);
//                                alertDialog.setView(dialogView);
//                                Button Back = (Button) dialogView.findViewById(R.id.okbtn);
//                                Back.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        //progressDialog.dismiss();
//                                        alertDialog.dismiss();
//                                    }
//                                });
//                                alertDialog.show();
//
//                            } else if (mSerialPosition.equals("222222")) {
//                                if(Build.VERSION.SDK_INT >=26){
//                                    vibrator.vibrate(VibrationEffect.createOneShot(200,VibrationEffect.DEFAULT_AMPLITUDE));
//                                }else {
//                                    vibrator.vibrate(200);
//                                }
//                                final AlertDialog alertDialog;
//                                alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
//                                LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
//                                View dialogView = inflater.inflate(R.layout.popupdealer, null);
//                                alertDialog.setView(dialogView);
//
//                                Button Back = (Button) dialogView.findViewById(R.id.okbtn);
//
//                                Back.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        //progressDialog.dismiss();
//                                        alertDialog.dismiss();
//                                    }
//                                });
//                                alertDialog.show();
//                            } else if (mSerialPosition.equals("3333333")) {
//                                final AlertDialog alertDialog;
//                                alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
//                                LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
//                                View dialogView = inflater.inflate(R.layout.successpopup, null);
//                                alertDialog.setView(dialogView);
//                                Button Back = (Button) dialogView.findViewById(R.id.okbtn);
//
//                                Back.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        //progressDialog.dismiss();
//                                        alertDialog.dismiss();
//                                        Intent i = new Intent(Serial_List_Activity.this,MechanicResult.class);
//                                        i.putExtra("mechincentiveamount","20");
//                                        i.putExtra("dateofscan","20/3/2019");
//                                        i.putExtra("dealername","");
//                                        i.putExtra("dealercity","");
//                                        i.putExtra("dealerstate","");
//                                        i.putExtra("alert","alert");
//                                        startActivity(i);
//
//                                    }
//                                });
//                                alertDialog.show();
//                            } else if (mSerialPosition.equals("9874566")) {
//
//                                final AlertDialog alertDialog;
//                                alertDialog = new AlertDialog.Builder(Serial_List_Activity.this).create();
//                                LayoutInflater inflater = ((Serial_List_Activity.this)).getLayoutInflater();
//                                View dialogView = inflater.inflate(R.layout.successpopup, null);
//                                alertDialog.setView(dialogView);
//                                Button Back = (Button) dialogView.findViewById(R.id.okbtn);
//
//                                Back.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        //progressDialog.dismiss();
//                                        alertDialog.dismiss();
//                                        Intent i = new Intent(Serial_List_Activity.this,MechanicResult.class);
//                                        i.putExtra("mechincentiveamount","20");
//                                        i.putExtra("dateofscan","20/3/2019");
//                                        i.putExtra("dealername","Velu");
//                                        i.putExtra("dealercity","Madurai");
//                                        i.putExtra("dealerstate","Tamilnadu");
//                                        i.putExtra("alert","noalert");
//                                        startActivity(i);
//                                    }
//                                });
//                                alertDialog.show();
//                            }else {
//                                StyleableToast st = new StyleableToast(Serial_List_Activity.this, "Select one serial number", Toast.LENGTH_SHORT);
//                                st.setBackgroundColor(Serial_List_Activity.this.getResources().getColor(R.color.red));
//                                st.setTextColor(Color.WHITE);
//                                st.setMaxAlpha();
//                                st.show();
//                            }
                        }
                        else {
                            StyleableToast st = new StyleableToast(Serial_List_Activity.this, "Select one serial number", Toast.LENGTH_SHORT);
                            st.setBackgroundColor(Serial_List_Activity.this.getResources().getColor(R.color.red));
                            st.setTextColor(Color.WHITE);
                            st.setMaxAlpha();
                            st.show();
                        }

                    }
        });

        NetworkConnection isnet = new NetworkConnection(Serial_List_Activity.this);
        if (isnet.CheckInternet()) {
            getCompleteSerialNO();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }
    private static okhttp3.OkHttpClient okClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(15, TimeUnit.MINUTES)
                .writeTimeout(15, TimeUnit.MINUTES)
                .readTimeout(15, TimeUnit.MINUTES)
                .build();
    }

    private void getCompleteSerialNO() {

        final ProgressDialog loading = ProgressDialog.show(Serial_List_Activity.this,
                "Loading", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUtils.BASE_URL_NEW)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<CheckPartId> filtersCall = api.checkPartId(mSixDigit);
        filtersCall.enqueue(new Callback<CheckPartId>() {
            @Override
            public void onResponse(Call<CheckPartId> call, Response<CheckPartId> response) {

                loading.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getResult().equals("success")) {
                        if (!response.body().getData().isEmpty()) {

                            ArrayList<String> seriList = new ArrayList<>();
                            List<CheckPartIdList> data = response.body().getData();
                            for (CheckPartIdList datum : data) {
                                seriList.add(datum.getPartid());
                            }
                            adapter =  new ArrayAdapter<String>(
                                    Serial_List_Activity.this,
                                    android.R.layout.simple_list_item_single_choice,
                                    seriList);
                            mListSerial.setAdapter(adapter);
                        }
                    } else box.showNegativebox("Serial Number not found!");
                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showNegativebox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CheckPartId> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showNegativebox(getResources().getString(R.string.server_error));

            }
        });

    }

    private void getSerialNO() {

        final ProgressDialog loading = ProgressDialog.show(Serial_List_Activity.this,
                "Loading", "Please wait...", false, false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiUtils.BASE_URL_NEW)
                .client(okClient())
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);
        Call<CheckPartId> filtersCall = api.checkPartId(mSixDigit);
        filtersCall.enqueue(new Callback<CheckPartId>() {
            @Override
            public void onResponse(Call<CheckPartId> call, Response<CheckPartId> response) {

                loading.dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getResult().equals("success")) {
                        if (!response.body().getData().isEmpty()) {

                            ArrayList<String> seriList = new ArrayList<>();
                            List<CheckPartIdList> data = response.body().getData();
                            for (CheckPartIdList datum : data) {
                                seriList.add(datum.getPartid());
                            }
                            adapter =  new ArrayAdapter<String>(
                                    Serial_List_Activity.this,
                                    android.R.layout.simple_list_item_single_choice,
                                    seriList);
                            mListSerial.setAdapter(adapter);
                        }
                    } else box.showNegativebox("Serial Number not found!");
                } else {
                    loading.dismiss();
                    // handle request errors yourself
                    box.showNegativebox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<CheckPartId> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showNegativebox(getResources().getString(R.string.server_error));

            }
        });

    }
}

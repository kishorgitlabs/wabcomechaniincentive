package com.brainmagic.wabcoincentive;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.textclassifier.TextLinks;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.URL;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;

import alertbox.Alertbox;
import api.APIService;

import api.ApiUtils;
import model.register.Register;
import model.register.RegisterData;
import model.response.OrderResponse;
import model.validateregistration.PostRegistration;
import network.NetworkConnection;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit.RetrofitClient3;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import toaster.Toasts;

public class RegisterActivity extends AppCompatActivity {
    private EditText nameEdit,phoneEdit,emailEdit,stateEdit,cityEdit,pincodeEdit,countryEdit,otpEdit,addressEdit,shopnameEdit;
    private LinearLayout afterotpLayout,beforeotpLayout;
    private LinearLayout retailerfieldsLayout;
    private String nameStr,phoneStr,emailStr,stateStr,cityStr,pincodeStr,countryStr,usertypeStr,App_ID,otpStr,Msg,strrr,otpafter,shopnameStr,addressStr;
    private Button registerBtn,generateotpBtn,validateotpBtn;
   private String[] state = {"Andaman and Nicobar", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh",
            "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
            "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur",
            "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana",
            "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal"};
 final ArrayList<String> liststate = new ArrayList<String>();
    /*   private List<String> statelist = new ArrayList<String>(Arrays.asList("Andaman and Nicobar", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh",
            "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh",
            "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur",
            "Meghalaya", "Mizoram", "Nagaland", "Odisha", "Puducherry", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Telangana",
            "Tripura", "Uttar Pradesh", "Uttarakhand", "West Bengal"));*/
    public ProgressDialog loadDialog;
    public NetworkConnection connection;
    private  Toasts toasts;
    private ProgressDialog progressDialog;
    private Context context;
    public Statement stmt;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    public Alertbox alertbox;
    private ArrayList<String> usertypeList;
    private MaterialSpinner  usertype;
    private static RegisterActivity instance;
    private Register registerData;
    private String selectionStr;
    private ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        retailerfieldsLayout = (LinearLayout) findViewById(R.id.retailerfiledslayout);
        addressEdit = (EditText)findViewById(R.id.addressedit);
        shopnameEdit = (EditText)findViewById(R.id.shopnameedit);
        otpEdit = (EditText)findViewById(R.id.otpedit);
        generateotpBtn = (Button) findViewById(R.id.generateotpbtn);
        validateotpBtn = (Button)findViewById(R.id.validateotp);
        progressDialog = new ProgressDialog(RegisterActivity.this);
        usertype = (MaterialSpinner) findViewById(R.id.usertypeedit);
        nameEdit =(EditText)findViewById(R.id.nameedit);
        phoneEdit = (EditText)findViewById(R.id.phoneedit);
        afterotpLayout = (LinearLayout)findViewById(R.id.afterotp);
        beforeotpLayout = (LinearLayout)findViewById(R.id.otpbefore);
        emailEdit = (EditText)findViewById(R.id.emailedit);
        stateEdit = (EditText)findViewById(R.id.stateedit);
        cityEdit = (EditText)findViewById(R.id.cityedit);
        pincodeEdit = (EditText)findViewById(R.id.pincodeedit);
        countryEdit = (EditText)findViewById(R.id.countryedit);
        registerBtn = (Button) findViewById(R.id.register);
        connection = new NetworkConnection(RegisterActivity.this);
        toasts = new Toasts(RegisterActivity.this);
        alertbox = new Alertbox(RegisterActivity.this);
        usertypeList = new ArrayList<String>();
        Collections.addAll(liststate, state);
         adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                liststate);
        usertypeList.add("Select Usertype");
        usertypeList.add("Retailer/Dealer");
        usertypeList.add("Mechanic");
        usertypeList.add("ASC");
        usertype.setBackgroundResource(R.drawable.autotextback);
        usertype.setItems(usertypeList);
        usertype.setPadding(30, 0, 0, 0);
        
        generateotpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(phoneEdit.getText().toString().equals(""))
                    phoneEdit.setError("Please Enter your mobile number");
                else if(phoneEdit.getText().toString().length() !=10)
                    phoneEdit.setError("Please Enter Valid mobile number");
                else
                generateOTP();
            }
        });

        usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                selectionStr = item.toString();
                if(selectionStr.equals("Retailer/Dealer")){
                    retailerfieldsLayout.setVisibility(View.VISIBLE);
                }else {
                    retailerfieldsLayout.setVisibility(View.GONE);
                }
            }
        });

        validateotpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!otpEdit.getText().toString().isEmpty()) {

                    otpafter = myshare.getString("CurrentOTP", "");
                    if (!(otpEdit.getText().toString().length() == 4 ) ) {

                        StyleableToast st = new StyleableToast(RegisterActivity.this,
                                "Invalid OTP", Toast.LENGTH_SHORT);
                        st.setBackgroundColor(getResources().getColor(R.color.red));
                        st.setTextColor(Color.WHITE);
                        st.setMaxAlpha();
                        st.show();
                    } else if (!(otpEdit.getText().toString().equals(otpafter))) {
                        StyleableToast st = new StyleableToast(RegisterActivity.this,
                                "Invalid OTP", Toast.LENGTH_SHORT);
                        st.setBackgroundColor(getResources().getColor(R.color.red));
                        st.setTextColor(Color.WHITE);
                        st.setMaxAlpha();
                        st.show();

                    }
                    else{
                        CheckInternet();
                    }

                } else {
                    StyleableToast st = new StyleableToast(RegisterActivity.this,
                            "Enter Your OTP", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }
            }
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              checkInternet();
            }
        });

        phoneEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                                if(s.toString().length() == 10){
                                    submitDetails();
                                }
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            stateEdit.setShowSoftInputOnFocus(false);
        }
        stateEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });
    }

    private void CheckInternet() {
        NetworkConnection isnet = new NetworkConnection(RegisterActivity.this);
        if (isnet.CheckInternet()) {
            UpdateUserInformation();
        } else {
            alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    private void UpdateUserInformation()
    {
        afterotpLayout.setVisibility(View.VISIBLE);
        beforeotpLayout.setVisibility(View.GONE);
    }

    private void generateOTP()
    {
        try
        {
            phoneStr = phoneEdit.getText().toString().trim();
            //  new SimpleOTPGenerator();
            otpStr = SimpleOTPGenerator.random(4);
            edit.putString("CurrentOTP", otpStr).commit();
            if (!otpStr.equals("")) {
                NetworkConnection isnet = new NetworkConnection(RegisterActivity.this);
                if (isnet.CheckInternet()) {
                    new sendOTPtoDatabase().execute();
                } else {
                    alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
                }
            }
//        Toast.makeText(this, "You Have Received OTP ", Toast.LENGTH_SHORT).show();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private class sendOTPtoDatabase extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            progressDialog.setMessage("Loading...");
        }

        @Override
        protected String doInBackground(String... strings) {
            int i = 0;
            Msg = "Dear " + "Customer" + ", Your OTP is "+otpStr+" Please enter this OTP to verify your mobile number.";
            try {
                URL url = new URL("http://sms.brainmagic.info/sendunicodesms?uname=WABCO&pwd=wabco123&senderid=WINSMS&msg=" + Msg + "&to=" + phoneStr + "&route=T");
                Log.v("URL", url.toString());



//                strrr = "http://sms.brainmagic.info/sendunicodesms?uname=WABCO&pwd=wabco123&senderid=WINSMS&msg=" + URLEncoder.encode(Msg, "UTF-8") + "&to=" + URLEncoder.encode(phoneStr, "UTF-8") + "&route=T";

                strrr = "http://sms.brainmagic.info/sendsms?uname=WABCO&pwd=wabco123&senderid=IHTSMS&to=" + URLEncoder.encode(phoneStr, "UTF-8") + "&msg=" + URLEncoder.encode(Msg, "UTF-8") + "&route=T&peid=1701159135033405399&tempid=0";



                Log.v("URL", strrr);
               /* HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(strrr);
                HttpResponse response = client.execute(request);*/

                OkHttpClient client = new OkHttpClient();
                Request.Builder builder = new Request.Builder();
                builder.url(strrr);
                Request request = builder.build();

                try {
                    okhttp3.Response response = client.newCall(request).execute();
                    return response.message();
                }catch (Exception e){
                    e.printStackTrace();
                }

                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals("success") || s.equals("OK")) {
//                sendOTPtoServer();
                progressDialog.dismiss();
                generateotpBtn.setVisibility(View.GONE);
                validateotpBtn.setVisibility(View.VISIBLE);
                Toast.makeText(RegisterActivity.this, "OTP sent to your mobile number.", Toast.LENGTH_SHORT).show();
            }
            else {
                progressDialog.dismiss();
                Toast.makeText(RegisterActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        }

    }
/*
    private void sendOTPtoServer() {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        // Creating object for our interface
        APIService api = retrofit.create(APIService.class);
        Call<OrderResponse> user = api.SentOTP(phoneStr, otpStr);
        user.enqueue(new Callback<OrderResponse>() {

            @Override
            public void onResponse(Call<OrderResponse> list, Response<OrderResponse> response) {
               */
/* if (response.isSuccessful()) {
                    OrderResponse user = response.body();
                    progressDialog.dismiss();
                    Log.v("Result", response.body().getResult());*//*

               progressDialog.dismiss();
                    if (response.body().getResult().equals("Success")) {
                        Toast.makeText(RegisterActivity.this, "OTP sent to your mobile number.", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResult().equals("NotFound")) {
                        Toast.makeText(RegisterActivity.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    } else {
                        alertbox.showAlertbox(getResources().getString(R.string.server_error));
                    }
                */
/* else {
                    progressDialog.dismiss();
                    alertbox.showAlertbox(getResources().getString(R.string.server_error));
                }*//*

            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                alertbox.showAlertbox(getResources().getString(R.string.server_error));
            }
        });

    }
*/

    private static class SimpleOTPGenerator {


        private SimpleOTPGenerator() {
        }

        private static String random(int size) {

            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                // Generate 20 integers 0..20
                for (int i = 0; i < size; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return generatedToken.toString();
        }
    }

    private void showPopup() {
        try {
            final android.app.AlertDialog alertDialog;
            alertDialog = new android.app.AlertDialog.Builder(this).create();
            LayoutInflater inflater = ((Activity) this).getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.popupcityadapter, null);
            alertDialog.setView(dialogView);
            final ListView list = (ListView) dialogView.findViewById(R.id.listdetails);
            list.setAdapter(adapter);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String s = list.getItemAtPosition(i).toString();
                    stateEdit.setText(s);
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //Check Internet-Connection
    private void checkInternet() {
        if(connection.CheckInternet()){
                validateFields();
        }else {
            alertbox.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    //Validate Fields
    private void validateFields() {
        nameStr = nameEdit.getText().toString().trim();
        phoneStr = phoneEdit.getText().toString().trim();
        emailStr = emailEdit.getText().toString().trim();
        stateStr = stateEdit.getText().toString().trim();
        cityStr = cityEdit.getText().toString().trim();
        pincodeStr = pincodeEdit.getText().toString().trim();
        countryStr = countryEdit.getText().toString().trim();
        usertypeStr = usertype.getText().toString();
        shopnameStr = shopnameEdit.getText().toString();
        addressStr = addressEdit.getText().toString();

        if(usertype.getText().toString().equals("Retailer/Dealer")){
            if (phoneStr.equals("")) {
                toasts.ShowErrorToast("Please Enter your Mobile number");
                phoneEdit.setError("Please Enter your Mobile number");
            } else if (phoneEdit.length() != 10) {
                toasts.ShowErrorToast("Please Enter Valid Mobile number");
                phoneEdit.setError("Please Enter Valid Mobile number");
            } else if (nameStr.equals("")) {
                nameEdit.setError("Please Enter your name");
                toasts.ShowErrorToast("Please Enter your name");
            } else if (emailStr.equals("")) {
                emailEdit.setError("Please Enter your Email");
                toasts.ShowErrorToast("Please Enter your Email");
            } else if (stateStr.equals("")) {
                stateEdit.setError("Please Enter your state");
                toasts.ShowErrorToast("Please Enter your state");
            } else if (cityStr.equals("")) {
                cityEdit.setError("Please Enter your city");
                toasts.ShowErrorToast("Please Enter your city");
            }else if(shopnameStr.equals("")) {
                toasts.ShowErrorToast("Please Enter Your Shopname");
                shopnameEdit.setError("Please Enter Your Shopname");
            }else if(addressStr.equals("")){
                toasts.ShowErrorToast("Please Enter Your Address");
                addressEdit.setError("Please Enter Your Address");
            }
            else if (usertype.getText().toString().equals("Select Usertype"))
                toasts.ShowErrorToast("Select your Usertype");
            else
                submitDetailstoNext();
        }else
            if (phoneStr.equals("")) {
                toasts.ShowErrorToast("Please Enter your Mobile number");
                phoneEdit.setError("Please Enter your Mobile number");
            } else if (phoneEdit.length() != 10) {
                toasts.ShowErrorToast("Please Enter Valid Mobile number");
                phoneEdit.setError("Please Enter Valid Mobile number");
            } else if (nameStr.equals("")) {
                nameEdit.setError("Please Enter your name");
                toasts.ShowErrorToast("Please Enter your name");
            } else if (emailStr.equals("")) {
                emailEdit.setError("Please Enter your Email");
                toasts.ShowErrorToast("Please Enter your Email");
            } else if (stateStr.equals("")) {
                stateEdit.setError("Please Enter your state");
                toasts.ShowErrorToast("Please Enter your state");
            } else if (cityStr.equals("")) {
                cityEdit.setError("Please Enter your city");
                toasts.ShowErrorToast("Please Enter your city");
            }
     /*   else if(emailStr.equals("") && nameStr.equals("") && stateStr.equals("") && cityStr.equals(""))
            toasts.ShowErrorToast("Please Enter Valid Mobile number");*/
            else if (usertype.getText().toString().equals("Select Usertype"))
                toasts.ShowErrorToast("Select your Usertype");

        else
            submitDetailstoNext();
    }


    private void submitDetailstoNext() {
       // Intent i = new Intent(RegisterActivity.this,MainActivity.class);
        edit.putBoolean("isregistration", true);
        edit.putString("name",nameStr);
        edit.putString("phone",phoneStr);
        edit.putString("email",emailStr);
        edit.putString("state",stateStr);
        edit.putString("city",cityStr);
        edit.putString("pincode",pincodeStr);
        edit.putString("country",countryStr);
        edit.putString("usertype",usertypeStr);
       // i.putExtra("usertype",usertypeStr);
       // i.putExtra("name",nameStr);
        edit.commit();
        postRegistration();
       // startActivity(i);
       /* Alertbox alertbox =  new Alertbox(RegisterActivity.this);
        alertbox.showNegativebox("Thank you for your Registration. Please Wait for Approval");*/
    }


    private void postRegistration() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "WABCO", "Please wait...", false, false);
        APIService service = RetrofitClient3.getApiService();
        Call<PostRegistration> call1 = service.POST_REGISTRATION_CALL(phoneStr,nameStr,emailStr,stateStr,cityStr,countryStr,pincodeStr,usertypeStr,myshare.getString("CurrentOTP",""),shopnameStr,addressStr);

        call1.enqueue(new Callback<PostRegistration>() {
            @Override
            public void onResponse(Call<PostRegistration> call, Response<PostRegistration> response) {
                try {
                    if(response.body().getResult().equals("Success")) {

                        Alertbox alertbox =  new Alertbox(RegisterActivity.this);
                        alertbox.showNegativebox("Thank you for your Registration. Please Wait for Approval.");

                    }
                    else if(response.body().getResult().equals("Not Success")){
                        // toasts.ShowErrorToast("Please Enter  Valid Mobile number");
                        alertbox.showNegativebox("Registration not success. please try again later");
                        loading.dismiss();
                    }else{
                        //  toasts.ShowErrorToast("User not Found");
//                        alertbox.showNegativebox(getString(R.string.server_error));

                        Alertbox alertbox =  new Alertbox(RegisterActivity.this);
                        alertbox.showNegativebox("Thank you for your Registration. Please Wait for Approval.");
                        loading.dismiss();
                    }
                }
                catch (Exception e) {
                    loading.dismiss();
                    e.printStackTrace();
                    alertbox.showNegativebox(getString(R.string.server_error));
                   // Toast.makeText(RegisterActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PostRegistration> call, Throwable t) {
                try {
                    loading.dismiss();
                    alertbox.showNegativebox(getString(R.string.server_error));
                    //  Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    //Toast.makeText(RegisterActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void submitDetails() {
        phoneStr = phoneEdit.getText().toString().trim();
        final ProgressDialog loading =
                ProgressDialog.show(this, "Verifying Details", "Please wait...", false, false);
        APIService service = RetrofitClient3.getApiService();
        Call<RegisterData> call1 = service.check_register(phoneStr);
        call1.enqueue(new Callback<RegisterData>() {
            @Override
            public void onResponse(Call<RegisterData> call, Response<RegisterData> response) {
                try {
                    if(response.body().getResult().equals("Success")) {
                        registerData = response.body().getData();
                        loading.dismiss();
                        successRegister(registerData);
                        nameEdit.setEnabled(true);
                        emailEdit.setEnabled(true);
                        stateEdit.setEnabled(true);
                        cityEdit.setEnabled(true);
                        pincodeEdit.setEnabled(true);
                        nameEdit.setFocusableInTouchMode(true);
                        emailEdit.setFocusableInTouchMode(true);
                        stateEdit.setFocusableInTouchMode(true);
                        cityEdit.setFocusableInTouchMode(true);
                        pincodeEdit.setFocusableInTouchMode(true);
                    }else if(response.body().getResult().equals("Not Success")){
                        // toasts.ShowErrorToast("Please Enter  Valid Mobile number");
                        nameEdit.setEnabled(true);
                        emailEdit.setEnabled(true);
                        stateEdit.setEnabled(true);
                        cityEdit.setEnabled(true);
                        pincodeEdit.setEnabled(true);
                        nameEdit.setFocusableInTouchMode(true);
                        emailEdit.setFocusableInTouchMode(true);
                        stateEdit.setFocusableInTouchMode(true);
                        cityEdit.setFocusableInTouchMode(true);
                        pincodeEdit.setFocusableInTouchMode(true);
                        loading.dismiss();
                    }else{
                        //  toasts.ShowErrorToast("User not Found");
                        nameEdit.setEnabled(true);
                        emailEdit.setEnabled(true);
                        stateEdit.setEnabled(true);
                        cityEdit.setEnabled(true);
                        pincodeEdit.setEnabled(true);
                        nameEdit.setFocusableInTouchMode(true);
                        emailEdit.setFocusableInTouchMode(true);
                        stateEdit.setFocusableInTouchMode(true);
                        cityEdit.setFocusableInTouchMode(true);
                        pincodeEdit.setFocusableInTouchMode(true);
                        loading.dismiss();
                    }

                }catch (Exception e) {
                    loading.dismiss();
                    e.printStackTrace();
                    Toast.makeText(RegisterActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<RegisterData> call, Throwable t) {
                try {
                    loading.dismiss();
                    //  Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    Toast.makeText(RegisterActivity.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void successRegister(Register registerData) {
        emailEdit.setText(registerData.getEmail());
        nameEdit.setText(registerData.getName());
        stateEdit.setText(registerData.getState());
        cityEdit.setText(registerData.getCity());
        pincodeEdit.setText(registerData.getPincode());
    }
}
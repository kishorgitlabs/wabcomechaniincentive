package com.brainmagic.wabcoincentive;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

public class ScanHistory extends AppCompatActivity {
        private ImageView backImg;
        private TextView textView_colorcode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_history);
        final ImageView sidemenu = (ImageView) findViewById(R.id.menu);
        backImg = (ImageView)findViewById(R.id.backbtn);
        textView_colorcode = (TextView)findViewById(R.id.viewcolorcode);
        textView_colorcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(view);
            }
        });
        backImg.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    });
        sidemenu.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(ScanHistory.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.barcode:
                                startActivity(new Intent(ScanHistory.this, BarcodeSelection.class));
                                break;
                            case R.id.wabcoincentive:
                                startActivity(new Intent(ScanHistory.this,MainActivity.class));
                        }
                        return false;
                    }
                });
                pop.inflate(R.menu.historymenu);
                pop.show();
            }
        });
    }

    private void showPopup(View view) {
        final AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(ScanHistory.this).create();
        LayoutInflater inflater = ((ScanHistory.this)).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.popupcolorcode, null);
        alertDialog.setView(dialogView);
        Button Back = (Button) dialogView.findViewById(R.id.okbtn);

        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //progressDialog.dismiss();
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }
}

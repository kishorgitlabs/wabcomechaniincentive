package logout;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.widget.Button;

import com.brainmagic.wabcoincentive.RegisterActivity;


import toaster.Toasts;


import static android.content.Context.MODE_PRIVATE;
import static android.support.constraint.Constraints.TAG;

/**
 * Created by Systems02 on 30-Oct-17.
 */

public class Logout {
    private Context context;
    Toasts toast;
    AlertDialog alertDialog;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;


    public Logout(Context context) {
        this.context = context;
        toast = new Toasts(context);
    }



    public void log_out()
    {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
       // builder1.setTitle("LucasTVS Network");
        builder1.setMessage("Are you want to Signout?");
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Log.d(TAG, "onClick: ");
                        myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
                        editor = myshare.edit();
                        editor.putBoolean("isregistration", false);
                        editor.apply();
                        context.startActivity(new Intent(context, RegisterActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                       // toast.ShowSuccessToast("Logout Successfully");

                       // alertDialog.dismiss();

                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

     /*   AlertDialog alert11 = builder1.create();
        alert11.show();
*/
        AlertDialog alert = builder1.create();
        alert.show();
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
       // nbutton.setBackgroundColor(Color.RED);
        nbutton.setTextColor(Color.BLACK);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        //pbutton.setBackgroundColor(Color.RED);
        pbutton.setTextColor(Color.BLACK);

    }
    }







package api;

import model.SerialList;
import model.checkPartId.CheckPartId;
import model.checkapproved.CheckApproved;
import model.geniunecheck.CheckGenuine;
import model.geniunecheck.CheckUnique;
import model.register.RegisterData;
import model.response.OrderResponse;
import model.validateregistration.PostRegistration;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface APIService {

    @FormUrlEncoded
    @POST("api/MOM/autoPartsearch")
    public Call<SerialList> GetautoPartsearch(
            @Field("partid") String partid);

    @FormUrlEncoded
    @POST("api/MOM/ScanedCheck")
    Call<CheckUnique> check_unique(
            @Field("partid") String SerialNumber);

    @FormUrlEncoded
    @POST("api/Values/ScanedCheck")
    Call<CheckUnique> checkScan(
            @Field("partid") String SerialNumber);
    @FormUrlEncoded
    @POST("api/Values/autoPartsearch")
    Call<CheckPartId> checkPartId(
            @Field("partid") String SerialNumber);

    @FormUrlEncoded
    @POST("api/MOM/GenuineCheckHis")
    Call<CheckGenuine> check_genuine(
            @Field("UniqueID") String UniqueID,
            @Field("SerialID") String SerialID,
            @Field("UserType") String UserType,
            @Field("userid") String id,
            @Field("pickid") String pickid,
            @Field("CustomerName") String CustomerName,
            @Field("TransactionAddress") String TransactionAddress,
            @Field("ScannedBy") String ScannedBy);

    //RegisterDetails
    @FormUrlEncoded
    @POST("api/Values/registeration")
    Call<RegisterData> check_register(
            @Field("phone") String Phonenumber);

    //CheckApproved
    @FormUrlEncoded
    @POST("api/Values/Checkupdate")
    Call<CheckApproved> CHECK_APPROVED_CALL (
            @Field("phone") String Phonenumber,
            @Field("usertype") String Usertype);

    //PostRegistration
    @FormUrlEncoded
    @POST("api/Values/registerPost")
    Call<PostRegistration> POST_REGISTRATION_CALL (
            @Field("phone") String Phonenumber,
            @Field("name") String name,
            @Field("email") String email,
            @Field("state") String state,
            @Field("city") String ciyt,
            @Field("country") String country,
            @Field("Pincode") String pincode,
            @Field("usertype") String Usertype,
            @Field("OTP") String otp,
            @Field("shopname") String shopname,
            @Field("address") String address);

    //smsotp
    @FormUrlEncoded
    @POST("api/MOM/SaveOTP")
    Call<OrderResponse> SentOTP(
            @Field("phone") String phonenumber,
            @Field("OTP")String mOtp);


}

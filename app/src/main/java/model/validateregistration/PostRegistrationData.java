
package model.validateregistration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PostRegistrationData {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dealercode")
    private String mDealercode;
    @SerializedName("deletestatus")
    private String mDeletestatus;
    @SerializedName("DeviceType")
    private String mDeviceType;
    @SerializedName("deviceid")
    private String mDeviceid;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Flag")
    private String mFlag;
    @SerializedName("gstnumber")
    private String mGstnumber;
    @SerializedName("id")
    private String mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("OTP")
    private String mOTP;
    @SerializedName("pannumber")
    private String mPannumber;
    @SerializedName("password")
    private String mPassword;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("shopname")
    private String mShopname;
    @SerializedName("state")
    private String mState;
    @SerializedName("username")
    private String mUsername;
    @SerializedName("usertype")
    private String mUsertype;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDealercode() {
        return mDealercode;
    }

    public void setDealercode(String dealercode) {
        mDealercode = dealercode;
    }

    public String getDeletestatus() {
        return mDeletestatus;
    }

    public void setDeletestatus(String deletestatus) {
        mDeletestatus = deletestatus;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String deviceType) {
        mDeviceType = deviceType;
    }

    public String getDeviceid() {
        return mDeviceid;
    }

    public void setDeviceid(String deviceid) {
        mDeviceid = deviceid;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFlag() {
        return mFlag;
    }

    public void setFlag(String flag) {
        mFlag = flag;
    }

    public String getGstnumber() {
        return mGstnumber;
    }

    public void setGstnumber(String gstnumber) {
        mGstnumber = gstnumber;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOTP() {
        return mOTP;
    }

    public void setOTP(String oTP) {
        mOTP = oTP;
    }

    public String getPannumber() {
        return mPannumber;
    }

    public void setPannumber(String pannumber) {
        mPannumber = pannumber;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getShopname() {
        return mShopname;
    }

    public void setShopname(String shopname) {
        mShopname = shopname;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}

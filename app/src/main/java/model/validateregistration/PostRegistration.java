
package model.validateregistration;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PostRegistration {

    @SerializedName("data")
    private PostRegistrationData mData;
    @SerializedName("result")
    private String mResult;

    public PostRegistrationData getData() {
        return mData;
    }

    public void setData(PostRegistrationData data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

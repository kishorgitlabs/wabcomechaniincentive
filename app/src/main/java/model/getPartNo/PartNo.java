
package model.getPartNo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartNo {

    @SerializedName("cuscode")
    private String mCuscode;
    @SerializedName("cusname")
    private String mCusname;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("id")
    private Long mId;
    @SerializedName("locationcode")
    private String mLocationcode;
    @SerializedName("mrp")
    private String mMrp;
    @SerializedName("mrpdate")
    private String mMrpdate;
    @SerializedName("partid")
    private String mPartid;
    @SerializedName("partnumber")
    private Long mPartnumber;
    @SerializedName("pickid")
    private String mPickid;
    @SerializedName("pickqty")
    private String mPickqty;
    @SerializedName("quantity")
    private String mQuantity;
    @SerializedName("randomno")
    private String mRandomno;
    @SerializedName("transaddress")
    private String mTransaddress;
    @SerializedName("transdate")
    private String mTransdate;
    @SerializedName("transid")
    private String mTransid;
    @SerializedName("updatedtime")
    private String mUpdatedtime;
    @SerializedName("userid")
    private Object mUserid;

    public String getCuscode() {
        return mCuscode;
    }

    public void setCuscode(String cuscode) {
        mCuscode = cuscode;
    }

    public String getCusname() {
        return mCusname;
    }

    public void setCusname(String cusname) {
        mCusname = cusname;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLocationcode() {
        return mLocationcode;
    }

    public void setLocationcode(String locationcode) {
        mLocationcode = locationcode;
    }

    public String getMrp() {
        return mMrp;
    }

    public void setMrp(String mrp) {
        mMrp = mrp;
    }

    public String getMrpdate() {
        return mMrpdate;
    }

    public void setMrpdate(String mrpdate) {
        mMrpdate = mrpdate;
    }

    public String getPartid() {
        return mPartid;
    }

    public void setPartid(String partid) {
        mPartid = partid;
    }

    public Long getPartnumber() {
        return mPartnumber;
    }

    public void setPartnumber(Long partnumber) {
        mPartnumber = partnumber;
    }

    public String getPickid() {
        return mPickid;
    }

    public void setPickid(String pickid) {
        mPickid = pickid;
    }

    public String getPickqty() {
        return mPickqty;
    }

    public void setPickqty(String pickqty) {
        mPickqty = pickqty;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRandomno() {
        return mRandomno;
    }

    public void setRandomno(String randomno) {
        mRandomno = randomno;
    }

    public String getTransaddress() {
        return mTransaddress;
    }

    public void setTransaddress(String transaddress) {
        mTransaddress = transaddress;
    }

    public String getTransdate() {
        return mTransdate;
    }

    public void setTransdate(String transdate) {
        mTransdate = transdate;
    }

    public String getTransid() {
        return mTransid;
    }

    public void setTransid(String transid) {
        mTransid = transid;
    }

    public String getUpdatedtime() {
        return mUpdatedtime;
    }

    public void setUpdatedtime(String updatedtime) {
        mUpdatedtime = updatedtime;
    }

    public Object getUserid() {
        return mUserid;
    }

    public void setUserid(Object userid) {
        mUserid = userid;
    }

}


package model.getPartNo;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class PartNoList {

    @SerializedName("data")
    private PartNo mData;
    @SerializedName("result")
    private String mResult;

    public PartNo getData() {
        return mData;
    }

    public void setData(PartNo data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

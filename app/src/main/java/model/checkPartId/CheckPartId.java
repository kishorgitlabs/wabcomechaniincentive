
package model.checkPartId;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckPartId {

    @SerializedName("data")
    private List<CheckPartIdList> mData;
    @SerializedName("result")
    private String mResult;

    public List<CheckPartIdList> getData() {
        return mData;
    }

    public void setData(List<CheckPartIdList> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

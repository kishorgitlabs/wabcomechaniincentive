
package model.checkPartId;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CheckPartIdList {

    @SerializedName("partid")
    private String mPartid;

    public String getPartid() {
        return mPartid;
    }

    public void setPartid(String partid) {
        mPartid = partid;
    }

}

package model.register;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Register {
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("country")
  @Expose
  private String country;
  @SerializedName("address")
  @Expose
  private String address;
  @SerializedName("city")
  @Expose
  private String city;
  @SerializedName("usertype")
  @Expose
  private String usertype;
  @SerializedName("gstnumber")
  @Expose
  private String gstnumber;
  @SerializedName("deviceid")
  @Expose
  private String deviceid;
  @SerializedName("pannumber")
  @Expose
  private String pannumber;
  @SerializedName("DeviceType")
  @Expose
  private String DeviceType;
  @SerializedName("password")
  @Expose
  private String password;
  @SerializedName("phone")
  @Expose
  private String phone;
  @SerializedName("shopname")
  @Expose
  private String shopname;
  @SerializedName("dealercode")
  @Expose
  private String dealercode;
  @SerializedName("name")
  @Expose
  private String name;
  @SerializedName("state")
  @Expose
  private String state;
  @SerializedName("deletestatus")
  @Expose
  private String deletestatus;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("email")
  @Expose
  private String email;
  @SerializedName("Pincode")
  @Expose
  private String Pincode;
  @SerializedName("username")
  @Expose
  private String username;
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setCountry(String country){
   this.country=country;
  }
  public String getCountry(){
   return country;
  }
  public void setAddress(String address){
   this.address=address;
  }
  public String getAddress(){
   return address;
  }
  public void setCity(String city){
   this.city=city;
  }
  public String getCity(){
   return city;
  }
  public void setUsertype(String usertype){
   this.usertype=usertype;
  }
  public String getUsertype(){
   return usertype;
  }
  public void setGstnumber(String gstnumber){
   this.gstnumber=gstnumber;
  }
  public String getGstnumber(){
   return gstnumber;
  }
  public void setDeviceid(String deviceid){
   this.deviceid=deviceid;
  }
  public String getDeviceid(){
   return deviceid;
  }
  public void setPannumber(String pannumber){
   this.pannumber=pannumber;
  }
  public String getPannumber(){
   return pannumber;
  }
  public void setDeviceType(String DeviceType){
   this.DeviceType=DeviceType;
  }
  public String getDeviceType(){
   return DeviceType;
  }
  public void setPassword(String password){
   this.password=password;
  }
  public String getPassword(){
   return password;
  }
  public void setPhone(String phone){
   this.phone=phone;
  }
  public String getPhone(){
   return phone;
  }
  public void setShopname(String shopname){
   this.shopname=shopname;
  }
  public String getShopname(){
   return shopname;
  }
  public void setDealercode(String dealercode){
   this.dealercode=dealercode;
  }
  public String getDealercode(){
   return dealercode;
  }
  public void setName(String name){
   this.name=name;
  }
  public String getName(){
   return name;
  }
  public void setState(String state){
   this.state=state;
  }
  public String getState(){
   return state;
  }
  public void setDeletestatus(String deletestatus){
   this.deletestatus=deletestatus;
  }
  public String getDeletestatus(){
   return deletestatus;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setEmail(String email){
   this.email=email;
  }
  public String getEmail(){
   return email;
  }
  public void setPincode(String Pincode){
   this.Pincode=Pincode;
  }
  public String getPincode(){
   return Pincode;
  }
  public void setUsername(String username){
   this.username=username;
  }
  public String getUsername(){
   return username;
  }
}
package model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class SerialData {
  @SerializedName("partid")
  @Expose
  private String partid;
  public void setPartid(String partid){
   this.partid=partid;
  }
  public String getPartid(){
   return partid;
  }
}